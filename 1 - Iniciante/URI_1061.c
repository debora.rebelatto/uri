#include <stdio.h>
#include <stdlib.h>

int main()
{
    int Di, Hi, Mi, Si, Df, Hf, Mf, Sf, W, X, Y, Z;
    int A, B, C, D, E, F[5];
    char a[10], b[10], c[10], a2[10], b2[10], c2[10];

    scanf("%s %d", &a, &Di);
    scanf("%d %s %d %s %d", &Hi, &b, &Mi, &c, &Si);
    scanf("%s %d", &a2, &Df);
    scanf("%d %s %d %s %d", &Hf, &b2, &Mf, &c2, &Sf);

    A = 60 - Si + Sf;
    B = (60 - Mi - 1 + Mf) * 60;
    C = (24 - 1 - Hi + Hf) * 3600;
    D = (Df - Di - 1) * 86400;
    E = A + B + C + D;

    F[0] = E / 86400;
    printf("%d dia(s)\n", F[0]);

    F[0] = E % 86400;
    F[1] = F[0] / 3600;
    printf("%d hora(s)\n", F[1]);

    F[1] = F[0] % 3600;
    F[2] = F[1] / 60;
    printf("%d minuto(s)\n", F[2]);
    
    F[2] = F[1] % 60;
    printf("%d segundo(s)\n", F[2]);
}
