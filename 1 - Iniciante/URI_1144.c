#include <stdio.h>
#include <stdlib.h>

int main()
{
    int N, a, b, c, x;
    scanf("%d", &N);
    for(; N > 0;)
    {
        x = N;
        for(a = 1; a <= x;)
        {
            b = a * a;
            c = a * a * a;
            printf("%d %d %d\n", a, b, c);
            printf("%d %d %d\n", a, b+1, c+1);
            a++;
            N--;
        }
    }
}
