#include <stdio.h>
#include <stdlib.h>

int main()
{
    int N, x, y, a, b, c;

    scanf("%d", &x);

    for(; x > 1;)
    {
        y = x;
        for(a = 1; a <= y;)
        {
            b = a * a;
            c = a * a * a;
            printf("%d %d %d\n", a, b, c);
            x--;
            a++;
        }

    }

}
