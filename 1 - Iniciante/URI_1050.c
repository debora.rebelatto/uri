#include <stdio.h>
#include <stdlib.h>

int main()
{
    char A1[] = "Brasilia", A2[] = "Salvador", A3[] = "Sao Paulo";
    char A4[] = "Rio de Janeiro", A5[] = "Juiz de Fora",  A6[] = "Campinas";
    char A7[] = "Vitoria", A8[] = "Belo Horizonte";
    int a;

    scanf("%d", &a);

    if(a == 61)
    {
        printf("%s\n", A1);
    }
    else if(a == 71)
    {
        printf("%s\n", A2);
    }
    else if(a == 11)
    {
        printf("%s\n", A3);
    }
    else if(a == 21)
    {
        printf("%s\n", A4);
    }
    else if(a == 32)
    {
        printf("%s\n", A5);
    }
    else if(a == 19)
    {
        printf("%s\n", A6);
    }
    else if(a == 27)
    {
        printf("%s\n", A7);
    }
    else if(a == 31)
    {
        printf("%s\n", A8);
    }
    else
    {
        printf("DDD nao cadastrado\n");
    }
}
