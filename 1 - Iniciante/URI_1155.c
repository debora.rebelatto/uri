#include <stdio.h>
#include <stdlib.h>

int main()
{
    double S = 0, a, b, c;

    for(a = 1; a <= 100; a++){
        c = 1 / a;
        S += c;
    }
    printf("%.2lf\n", S);
}
