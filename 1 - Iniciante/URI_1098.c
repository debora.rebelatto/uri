#include <stdio.h>
#include <stdlib.h>

int main()
{
    double I, J, x;
    int y;

    for(I = 0; I <= 1.9; I += .2){
        for(J = 1; J <= 3.0; J++){
            x = I + J;
            if(I == 0 || I == 1.0){
                printf("I=%.0lf J=%.0lf", I, x);
                }else if(x == 3 || x == 4.0 || x == 5.0){
                    printf("I=%.0lf J=%.0lf", I, x);
                    }else{
                        printf("I=%.1lf J=%.1lf", I, x);
                    }
                    printf("\n");
        }
    }
    for(y = 3; y <= 5; y++){
        printf("I=2 J=%d\n", y);
    }
}
