#include <stdio.h>
#include <stdlib.h>

int main()
{
    int H1, M1, H2, M2;
    int dif = 0;
    while(4 == scanf("%d %d %d %d", &H1, &M1, &H2, &M2)){
        if(H1 + H2 + M1 + M2 == 0){
            break;
        }
        dif = (H2 - H1) * 60 + (M2 - M1);

        if(dif < 0){
            dif += 24 * 60;
        }
        printf("%d\n", dif);
    }
    return 0;

}
