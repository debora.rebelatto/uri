#include <stdio.h>
#include <stdlib.h>

int main()
{
    double S = 0, a, b, c;

    for(a = 1, b = 1; a <= 39; a += 2){
        c = a / b;
        S += c;
        b *= 2;
    }
    printf("%.2lf\n", S);

}
