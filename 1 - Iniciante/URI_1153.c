#include <stdio.h>
#include <stdlib.h>

int main()
{
    int N, x;

        scanf("%d", &N);

        x = N - 1;

        for(; x > 1; x--)
        {
            N *= x;
        }
        printf("%d\n", N);


}
