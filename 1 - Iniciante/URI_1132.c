#include <stdio.h>
#include <math.h>
int main()
{
    int X, Y, i = 0, j = 0;

    scanf("%d %d", &X, &Y);

    if(X < Y)
    {
        for(i = X; i <= Y; i++)
        {
            if(i % 13 != 0)
            {
                j += i;
            }
        }
    }
    else if(X > Y)
    {
        for(i = Y; i <= X; i++)
        {
            if(i % 13 != 0)
            {
                j += i;
            }
        }
    }
    printf("%d\n", j);
}
