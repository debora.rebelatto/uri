#include <stdio.h>
#include <stdlib.h>
int main()
{
    double a, b, c, d;
    int T;

    scanf("%d", &T);

    for(; T >= 1; T--)
    {
        scanf("%lf %lf %lf", &a, &b, &c);

        d = (a * 0.2) + (b * 0.3) + (c * 0.5);

        printf("%.1lf\n", d);
    }

}

