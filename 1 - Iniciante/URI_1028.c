#include <stdio.h>
int main()

{
    int a, b, c, d, e, x;

    scanf("%d", &x);

    for(; x >= 1; x--)
    {
        scanf("%d %d", &a, &b);

        if(b > a)
        {
            d = b;
            e = a;
        }
        else
        {
            d = a;
            e = b;
        }

        while(d % e != 0)
        {
            c = d % e;
            d = e;
            e = c;
        }
        printf("%d\n", e);
    }
}
