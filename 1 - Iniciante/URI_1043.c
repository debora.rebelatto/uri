#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    float a, b, c, area, per;
    scanf("%f %f %f", &a, &b, &c);
    if (a < b + c && b < a + c && c < a + b)
    {
        per = a + b + c;
        printf("Perimetro = %.1f\n", per);
    }else{
        area = c * (a + b)/ 2;
        printf("Area = %.1f\n", area);
    }
}
