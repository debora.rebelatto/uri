#include <stdio.h>
#include <stdlib.h>

int main()
{
    int T, B, Ai, Di, Li, Vi, Af, Df, Lf, Vf;

    scanf("%d", &T);

    for(; T >= 1; T--){
        scanf("%d", &B);
        scanf("%d %d %d", &Ai, &Di, &Li);
        scanf("%d %d %d", &Af, &Df, &Lf);

        Vi = (Ai + Di) / 2;
        Vf = (Af + Df) / 2;

        if(Li % 2 == 0){
            Vi = Vi + B;
            }
        if(Lf % 2 == 0){
                Vf = Vf + B;
            }

        if(Vi == Vf){
            printf("Empate\n");
            }else printf((Vi > Vf)? "Dabriel\n" : "Guarte\n");
    }
}
