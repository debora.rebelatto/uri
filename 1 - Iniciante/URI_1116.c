#include <stdio.h>
#include <math.h>
int main()
{
    int N;
    float X, Y;

    scanf("%d", &N);
    for(; N >= 1; N--){

        scanf("%f %f", &X, &Y);

        if(Y == 0)
        {
            printf("divisao impossivel\n");
        }
        else
        {
            X /= Y;
            printf("%.1f\n", X);
        }
    }
}
