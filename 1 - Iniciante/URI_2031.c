#include <stdio.h>
#include <stdlib.h>

int main()
{
    char pe[]="pedra", pa[]="papel", at[]="ataque";
    char a[100], b[100];
    int i;
    scanf("%d", &i);

    for(; i >= 1; i--)
    {
        scanf("%s %s", a, b);

        if(0==strcmp(a, at))
        {
            if(0==strcmp(b, at))
            {
                printf("Aniquilacao mutua\n");
            }
            else if(0==strcmp(b, pa) || 0 == strcmp(b, pe))
            {
                printf("Jogador 1 venceu\n");
            }
        }

        else if(0==strcmp(a, pe))
        {
            if(0==strcmp(b, at))
            {
                printf("Jogador 2 venceu\n");
            }
            else if(0==strcmp(b, pa))
            {
                printf("Jogador 1 venceu\n");
            }
            else if(0==strcmp(b, pe))
            {
                printf("Sem ganhador\n");
            }
        }
        else if(0 == strcmp(a, pa))
        {
            if(0==strcmp(b, pe)||0==strcmp(b, at))
            {
                printf("Jogador 2 venceu\n");
            }
            else if(0==strcmp(b, pa))
            {
                printf("Ambos venceram\n");
            }
        }
    }

}
