#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
int main()
{
    int x, y, L;
    int BSH, IVH, EVH;
    int BSA, IVA, EVA;
    int BSD, IVD, EVD;
    int BSS, IVS, EVS;
    int HP, AT, DF, SP;
    char poke[100];

    //Bs valor base do atributo
    //EV valor dos esforços
    //IV valor individual

    scanf("%d", &x);

    for(y = 1; y <= x; y++){
        scanf("%s %d", poke, &L);
        scanf("%d %d %d", &BSH, &IVH, &EVH);
        scanf("%d %d %d", &BSA, &IVA, &EVA);
        scanf("%d %d %d", &BSD, &IVD, &EVD);
        scanf("%d %d %d", &BSS, &IVS, &EVS);

        HP = (((IVH + BSH + sqrt(EVH) / 8 + 50) * L) / 50) + 10;
        AT = (((IVA + BSA + sqrt(EVA) / 8) * L) / 50) + 5;
        DF = (((IVD + BSD + sqrt(EVD) / 8) * L) / 50) + 5;
        SP = (((IVS + BSS + sqrt(EVS) / 8) * L) / 50) + 5;

        printf("Caso #%d: %s nivel %d\n", y, poke, L);
        printf("HP: %d\n", HP);
        printf("AT: %d\n", AT);
        printf("DF: %d\n", DF);
        printf("SP: %d\n", SP);
    }
}
