#include <stdio.h>
#include <stdlib.h>

int main()
{
    int X, N=0, a=0, b=0;
    scanf("%d", &N);
    do{
        scanf("%d", &X);
        N--;
        if(X>0&&X%2==0) printf("EVEN POSITIVE\n");
        if(X<0&&X%2==0) printf("EVEN NEGATIVE\n");
        if(X>0&&X%2!=0) printf("ODD POSITIVE\n");
        if(X<0&&X%2!=0) printf("ODD NEGATIVE\n");
        if(X==0) printf("NULL\n");
    }while(N!=0);
}
