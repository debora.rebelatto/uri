#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
    float x1, x2, y1, y2, a, b, c, dis;
    scanf("%f %f", &x1, &y1);
    scanf("%f %f", &x2, &y2);
    a = x2-x1;
    b = y2-y1;
    c = (a * a) + (b * b);
    dis = sqrt(c);
    printf("%.4f\n", dis);
}
