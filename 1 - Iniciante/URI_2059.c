#include <stdio.h>
#include <stdlib.h>

int main()
{
    int p, j1, j2, r, a;
    /*  p  = Escolha do jogador 1
            1 = par 0 = ímpar
        j1 = Número do jogador 1
        j2 = Número do jogador 2
        r  = Se jogador 1 roubou
            1 = roubou 0 = não
        a  = Se jogador 2 acusou
            1 = acusou 0 = não  */

    scanf("%d %d %d %d %d", &p, &j1, &j2, &r, &a);

    j1 += j2;
    j1 %= 2;

    if(p == 1)
    {
        if(r == 1 && a == 0)
        {
            printf("Jogador 1 ganha!\n");
        }
        else if(r == 1 && a == 1)
        {
            printf("Jogador 2 ganha!\n");
        }
        else if(r == 0 && a == 1)
        {
            printf("Jogador 1 ganha!\n");
        }
        else if(j1 == 0)
        {
            printf("Jogador 1 ganha!\n");
        }
        else
        {
            printf("Jogador 2 ganha!\n");
        }
    }
    else if(p == 0)
    {
        if(r == 1 && a == 0)
        {
            printf("Jogador 1 ganha!\n");
        }
        else if(r == 1 && a == 1)
        {
            printf("Jogador 2 ganha!\n");
        }
        else if(r == 0 && a == 1)
        {
            printf("Jogador 1 ganha!\n");
        }
        else if(j1 == 0)
        {
            printf("Jogador 2 ganha!\n");
        }
        else
        {
            printf("Jogador 1 ganha!\n");
        }
    }
}
